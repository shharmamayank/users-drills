function Problem4(users) {
    let arr = []
    if (users === undefined || users.constructor != Array || users.length === 0) {
        return arr
    }
    for (let index = 0; index < users.length; index++) {
        arr.push(users[index].email)
    }
    return arr
}
module.exports = Problem4