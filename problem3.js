function Problem3(users) {
    let arr = []
    if (users === undefined || users.constructor != Array || users.length === 0) {
        return arr
    }

    return users.sort(function (a, b) {
        return a.last_name.localeCompare(b.last_name)
    })


}


module.exports = Problem3