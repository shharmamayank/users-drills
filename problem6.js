function problem6(users) {
    let arr = []
    if (users === undefined || users.constructor != Array || users.length === 0) {
        return arr
    } else {
        for (let index = 0; index < users.length; index++) {

            if ((users[index].gender == "Male") || (users[index].gender == "Female") || (users[index].gender == "Polygender") || (users[index].gender == "Bigender") || (users[index].gender == "Genderqueer") || (users[index].gender == "Genderfluid") || (users[index].gender == "Agender")) {
                arr.push(users[index])
            }
        }
        return JSON.stringify(arr)
    }
}
module.exports = problem6