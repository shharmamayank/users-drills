function Problem1(users, passId) {
    let arr = []
    if (users === undefined || users.constructor != Array || users.length === 0) {
        return arr
    }
    else {
        for (let index = 0; index < users.length; index++) {
            if (users[index].id === passId) {
                return users[index];
            }
        }
        return arr
    }
}
module.exports = Problem1